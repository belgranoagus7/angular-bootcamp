import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './users/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-App';

  constructor(private router: Router) { }

  goToUsers(){
    this.router.navigate(['/users']);
  }

}
