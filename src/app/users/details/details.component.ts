import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user.models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit{

  // user: User | undefined;
  @Input() user: User = {} as User;
  @Output() salida: EventEmitter<any> = new EventEmitter()

  constructor(private _userService: UserService) { }

  ngOnInit(): void {
    // this._userService.getUserById(1).subscribe({
    //   next: (x) => {
    //     this.user = x;
    //    }
    // })
  }

  emitirTexto(text: any){
    console.log(text.target.value);
    this.salida.emit(text.target.value)
  }
}
