import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user.models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {
  
  displayedColumns: any[] = ['id', 'name', 'email', 'phone', 'config'];
  listUsers: User[] = [];
  idUser: number | undefined;  
  userEmitter: User = {} as User;
  recibido: string = '';
  
  constructor(private _userService: UserService, private router: Router) { }

    ngOnInit(): void {
    this.getUsers();
  }

  getUsers(){
    this._userService.getUsers().subscribe({
      next: (x: User[]) => {
        this.listUsers = x;
        console.log(this.listUsers as User[]);        
      }
    });
  }

  deleteUser(id: number){
    this._userService.deleteUserById(id).subscribe({
      next: () => {
        this.listUsers = this.listUsers?.filter(x => x.id != id);
        //this.getUsers(); esto es lo correcto
      },
      error: () => alert("hubo un error al eliminar")
    });
  }

  goToEdit(id: number){
    this.router.navigate([`/users/modificacion/${id}`]);
  }

  verDetails(element: User){
    this.userEmitter = element;
  }
}
