import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormControl, FormGroup} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-modificacion',
  templateUrl: './modificacion.component.html',
  styleUrls: ['./modificacion.component.css']
})

export class ModificacionComponent implements OnInit {
  
  form: FormGroup = new FormGroup({
    'id': new FormControl(),
    'name': new FormControl(),
    'email': new FormControl(),
    'phone': new FormControl(),
  })
  id!: number;
  
  constructor(private _route: ActivatedRoute, private _service: UserService, private router: Router) { }
      
  ngOnInit(): void {
    this.id = this._route.snapshot.params["id"];
    console.log(this.id);
    if(Number(this.id)){
      this._service.getUserById(Number(this.id)).subscribe({
        next: (us) => {
          this.form.controls['id'].disable();
          this.form.controls['id'].patchValue(us.id)         
          this.form.controls['name'].patchValue(us.name)        
          this.form.controls['email'].patchValue(us.email)        
          this.form.controls['phone'].patchValue(us.phone)        
        },
        error: (err) => alert("Hubo un problema al obtener el usuario")        
      })
    }else{
      alert("ID inválida");      
    }
  }
  
  modificarUsuario(){
    let user = this.form.value;
    this._service.putUser(this.id, user).subscribe({
      next: () => {
        alert("TODO OK")
      },
      error: (err) => alert("Hubo un problema al obtener el usuario"),
      complete: () => this.router.navigate(['/users'])
    })
    
  }
}