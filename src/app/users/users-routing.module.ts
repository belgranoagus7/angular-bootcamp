import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleComponent } from '../albums/detalle/detalle.component';
import { AltaComponent } from './alta/alta.component';  
import { DetailsComponent } from './details/details.component';
import { ListComponent } from './list/list.component';
import { ModificacionComponent } from './modificacion/modificacion.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent
  },
  {
    path: 'alta',
    component: AltaComponent
  },
  {
    path: 'modificacion/:id',
    component: ModificacionComponent
  },
  {    
    path: 'details/:id',
    component: DetailsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
 