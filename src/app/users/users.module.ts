import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListComponent } from './list/list.component';
import { AltaComponent } from './alta/alta.component';
import { ModificacionComponent } from './modificacion/modificacion.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { DetailsComponent } from './details/details.component';
import { UtilsModule } from '../utils/utils.module';


@NgModule({
  declarations: [
    ListComponent,
    AltaComponent,
    ModificacionComponent,
    DetailsComponent,
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    HttpClientModule,
    UtilsModule
  ],
  providers: [
    UserService
  ]
})
export class UsersModule { }
