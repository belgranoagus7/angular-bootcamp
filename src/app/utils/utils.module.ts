import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatTableModule} from '@angular/material/table';
import { MatSliderModule } from "@angular/material/slider";
import {MatInputModule} from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import {MatIconModule} from '@angular/material/icon';


const modules = [
    MatTableModule,
    MatSliderModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule    
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ...modules,
    ],
    exports: [
        ...modules
    ]
})
export class UtilsModule { }